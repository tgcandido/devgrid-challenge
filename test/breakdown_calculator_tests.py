import sys
sys.path.append('..')
import unittest
from datetime import time
from breakdown_calculator import BreakdownCalculator
from sensor_event import SensorEvent
from appliance import Appliance


class BreakdownCalculatorTests(unittest.TestCase):
    def setUp(self):
        self.sensor_events = ([SensorEvent(time(0, 15), [0, 0, 0, 0, 0]), SensorEvent(time(0, 30), [1, 1, 1, 2, 1]), 
            SensorEvent(time(0, 45), [2, 3, 20, 2, -2]), SensorEvent(time(1, 15), [3, 3, 30, 2, 30]), 
            SensorEvent(time(1, 25), [4, 4, 40, 2, -40]), SensorEvent(time(1, 35), [6, 6, 60, 2, 60]), 
            SensorEvent(time(2, 15), [7, 9, 77, 2, 70]), SensorEvent(time(2, 30), [9, 9, 99, 2, -90]), 
            SensorEvent(time(2, 45), [12, 12, 102, 2, 120]), SensorEvent(time(3, 15), [13, 17, 104, 2, 144]), 
            SensorEvent(time(3, 25), [15, 18, 107, 2, 155]), SensorEvent(time(3, 35), [20, 25, 220, 2, 206]), 
            SensorEvent(time(4, 15), [22, 28, 222, 2, 207]), SensorEvent(time(4, 30), [20, 29, 230, 2, 208]), 
            SensorEvent(time(4, 45), [30, 30, 233, 2, 239]), SensorEvent(time(5, 15), [-31, 32, 234, 2, 309, 0]), 
            SensorEvent(time(5, 25), [-33, 33, 235, 3, 339, 1]), SensorEvent(time(5, 35), [42, 42, 242, 3, 429, 1]), 
            SensorEvent(time(6, 15), [44, 50, 244, 3, 449, 34]), SensorEvent(time(6, 30), [46, 55, 245, 3, 459, 35]), 
            SensorEvent(time(6, 45), [56, 65, 256, 3, 569, 36]), SensorEvent(time(7, 15), [57, 70, 259, 3, 589, 40]), 
            SensorEvent(time(7, 25), [60, -71, 266, 3, 609, 41]), SensorEvent(time(7, 35), [72, 82, 272, 3, -729, 45]), 
            SensorEvent(time(8, 15), [73, 83, 274, 3, -759, 45]), SensorEvent(time(8, 30), [76, 85, 276, 3, 769, 45]), 
            SensorEvent(time(8, 35), [78, 86, -278, 3, -789, 45]), SensorEvent(time(8, 38), [80, 87, 281, 3, 839, 45]), 
            SensorEvent(time(8, 48), [90, 120, 290, 3, 909, 45])])

    def test_calculate(self):
        appliance = Appliance(0, "Tv", 100)
        calculator = BreakdownCalculator()
        result_list = calculator.calculate(self.sensor_events, appliance)
        self.assertListEqual(result_list, 
            [2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])


if __name__ == '__main__':
    unittest.main()
