import sys
sys.path.append('..')
import unittest
from datetime import time
from sensor_event import SensorEvent  

class SensorEventTests(unittest.TestCase):
    def test_init_with_invalid_param_created_at(self):
        created_at = 12345
        appliances = []
        self.assertRaises(ValueError, lambda: SensorEvent(created_at, appliances))

    def test_init_with_invalid_param_appliances(self):
        created_at = time(16,40,00)
        appliances = 12345
        self.assertRaises(ValueError, lambda: SensorEvent(created_at, appliances))
    
    def test_init_with_invalid_param_too_many_appliances(self):
        created_at = time(16,40,00)
        appliances = [0] * 100
        self.assertRaises(ValueError, lambda: SensorEvent(created_at, appliances))

    def test_init_with_valid_params(self):
        created_at = time(16,40,00)
        appliances = [1,2,3,4]
        
        sensor = SensorEvent(created_at, appliances)

        self.assertEqual(sensor.created_at, created_at)
        self.assertEqual(sensor.appliances, appliances + [0,0,0,0,0,0])

if __name__ == '__main__':
    unittest.main()