from datetime import time

class SensorEvent(object):
    def __init__(self, created_at, appliances):
        if not isinstance(created_at, time):
            raise ValueError('created_at must be a datetime.time instance')
        elif not isinstance(appliances, list):
            raise ValueError('appliances must be a list')
        elif len(appliances) > 10:
            raise ValueError('too many appliances')
        else:
            temp = [0] * 10
            self.appliances = appliances + temp[len(appliances):10]
            self.created_at = created_at
            