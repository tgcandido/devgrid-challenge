from sensor_event import SensorEvent
from datetime import time
from itertools import groupby

class BreakdownCalculator(object):
    def __init__(self):
        self.sensor_events = { 
            0: [SensorEvent(time(0,0,0), [])],
            1: [SensorEvent(time(1,0,0), [])],
            3: [SensorEvent(time(2,0,0), [])],
            2: [SensorEvent(time(3,0,0), [])],
            4: [SensorEvent(time(4,0,0), [])],
            5: [SensorEvent(time(5,0,0), [])],
            6: [SensorEvent(time(6,0,0), [])],
            7: [SensorEvent(time(7,0,0), [])],
            8: [SensorEvent(time(8,0,0), [])],
            9: [SensorEvent(time(9,0,0), [])],
            10: [SensorEvent(time(10,0,0), [])],
            12: [SensorEvent(time(11,0,0), [])],
            11: [SensorEvent(time(12,0,0), [])],
            13: [SensorEvent(time(13,0,0), [])],
            14: [SensorEvent(time(14,0,0), [])],
            15: [SensorEvent(time(15,0,0), [])],
            16: [SensorEvent(time(16,0,0), [])],
            17: [SensorEvent(time(17,0,0), [])],
            18: [SensorEvent(time(18,0,0), [])],
            19: [SensorEvent(time(19,0,0), [])],
            20: [SensorEvent(time(20,0,0), [])],
            21: [SensorEvent(time(21,0,0), [])],
            22: [SensorEvent(time(22,0,0), [])],
            23: [SensorEvent(time(23,0,0), [])]
        }

    def calculate(self, sensor_events, appliance):
        self.__update_sensor_events(sensor_events)
        highest_consumption = self.__get_highest_hourly_consumption(appliance)
        adjacent_pairs = self.__get_adjacent_pairs(highest_consumption)
        return self.__get_result_list(adjacent_pairs, appliance)

    def __update_sensor_events(self, sensor_events):
       grouped_by_hour = dict([(key, list(events)) for key, events in groupby(sensor_events, key = lambda sensor_event : sensor_event.created_at.hour)])
       self.sensor_events = dict(list(self.sensor_events.items()) + list(grouped_by_hour.items()))

    def __get_highest_hourly_consumption(self, appliance):
        return map(lambda (key, value): (key, max(map(lambda sensor_event: sensor_event.appliances[appliance.id], value))), self.sensor_events.iteritems())

    def __get_adjacent_pairs(self, highest_consumption):
        return[(highest_consumption[i -1] if i > 0 else None, current) for i, current in enumerate(highest_consumption)]

    def __get_result_list(self, adjacent_pairs, appliance):
        hourly_consumption = map(lambda pair: (
            pair[1][0], pair[1][1] - pair[0][1] if pair[1][1] > pair[0][1] else 0) 
            if pair[0] is not None else (pair[1][0], pair[1][1]), adjacent_pairs)
        pair_list = map(lambda pair: (pair[0], round((pair[1] / float(appliance.power))*100, 2)), hourly_consumption)
        result = [value for (hour, value) in pair_list]
        return result